<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assests/css/dashboard-css.min.css') ?> ">
<!------ Include the above in your HEAD tag ---------->
<?php 
$uid = intval($this->session->userdata('id'));
?>

<html>
<head>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet"

</head>
<body>
	<div class="container">
		<h3 class=" text-center">Messaging</h3>
		<div class="messaging">
			<div class="inbox_msg">
				<div class="inbox_people">
					<div class="headind_srch">
						<div class="recent_heading">
							<h4>Recent</h4>
						</div>
						<div class="srch_bar">
							<div class="stylish-input-group">
								<a class="recent_heading" href="<?php echo base_url('user/logout') ?>">Log out</a>
							</div>
						</div>
					</div>
					<div class="inbox_chat">
						<?php foreach ($data as $key => $data): ?>
							<a href="<?php echo base_url('user/getMsg/').$data->id."/".$uid; ?>">
								<div class="chat_list ">
									<div class="chat_people">
										<div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
										<div class="chat_ib">
											<h5><?php echo $data->username ?><span class="chat_date">Dec 25</span></h5>
											<p>Test, which is a new approach to have all solutions 
											astrology under one roof.</p>
										</div>
									</div>
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="mesgs">

					<div class="msg_history" id="result" onLoad="scroll()">
            <!-- <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>Test which is a new approach to have all
                    solutions</p>
                  <span class="time_date"> 11:01 AM    |    June 9</span></div>
              </div>
            </div>
            <div class="outgoing_msg">
              <div class="sent_msg">
                <p>Test which is a new approach to have all
                  solutions</p>
                <span class="time_date"> 11:01 AM    |    June 9</span> </div>
            </div> -->
            <?php foreach ($msg as $key => $msg) : ?>
            	
            	<?php 
            	if ($msg->sender == $uid) {
            		echo '<div class="outgoing_msg">
            		<div class="sent_msg">
            		<p>'.$msg->message.'</p>
            		<span class="time_date">';
            		echo $msg->time ;
            		echo
            		'</span> 
            		</div>
            		</div>' ;
            	}
            	else {

            		echo 
            		'<div class="incoming_msg">
            		<div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
            		<div class="received_msg">
            		<div class="received_withd_msg">
            		<p>';
            		echo $msg->message;
            		echo
            		'</p>
            		<span class="time_date">'.$msg->time.'</span></div>
            		</div>
            		</div>';
            	}
            	?>
            <?php endforeach; ?>
            <script type="text/javascript">
            	// $(document).ready(function(){
            	// 	var objPerson = {
            	// 		test: function(){
            	// 			$('#result').animate({scrollTop: $('#result')[0].scrollHeight}, 0);
            	// 		}
            	// 	};
            	// 	$("#result").mouseenter($.proxy(objPerson, "test"));
            	// });
            	setInterval("my_function();",5000); 
            	function my_function(){
            		$('#result').load(location.href + ' #result');
            	}
            	function scroll(){
            		$('#result').animate({scrollTop: $('#result')[0].scrollHeight}, 0);
            	}
            	// $("#result").load(function(){
            	// 	$('#result').animate({scrollTop: $('#result')[0].scrollHeight}, 0);
            	// });

            </script>
        </div>
        <form autocomplete="off" method="post" action="<?php echo base_url('user/chat/').$uid."/".$friend ?> ">
        	<div class="type_msg">
        		<div class="input_msg_write">
        			<input id="input" type="text" name="msg" class="write_msg" placeholder="Type a message" value="" />
        			<button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
        			<!-- <input type="submit" class="msg_send_btn"><i class="fa fa-paper-plane-o" aria-hidden="true" ></i> -->
        		</div>
        	</div>
        </form>
    </div>
</div>


<p class="text-center top_spac"> Design by <a target="_blank" href="#">US.SOFT</a></p>

</div></div>
</body>

<!-- <script type="text/javascript">
	$(document).ready(function(){
		
	});
</script> -->
<script type="text/javascript">
	
</script>
<script type="text/javascript">
	// $(document).ready(function(){
	// 	setInterval("my_function();",5200); 
	// 	function my_function(){
	// 		$('#result').animate({scrollTop: $('#result')[0].scrollHeight}, 0);
	// 	}	
	// });
	
</script>
</html>